import './style.css';
import * as THREE from 'three';
import { ARButton } from '/src/ARButton.js';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { AnimationMixer } from 'three';

// global variables
let camera, scene, renderer;
let controller;
let rotationSpeed = [];
let scaleFragment = [];
let directionX = [], directionY = [], directionZ = [];
let rootBaseLine = null;
let rootDetailLine = null;
let rootSmallDetail = null;
let meshBaseLine = null;
let scale = 0;

// texture loader
const texturelLoader = new THREE.TextureLoader();

// animation
let mixerBase;


function init() {

    // scenario build
	const container = document.createElement('div');
	document.body.appendChild(container);
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 100);
    const light = new THREE.HemisphereLight(0xffffff, 0xbbbbff, 1.25);
	//light.position.set(0.5, 3, 0.25);
	scene.add(light);
	renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.xr.enabled = true;
	container.appendChild(renderer.domElement);
	document.body.appendChild(ARButton.createButton(renderer));

    // gltf loader
	const gltfLoader = new GLTFLoader();
    const fbxLoader = new FBXLoader();

    // CONTROLLERS FOR SCALE AND POSITION
    let masterScale = 1;
    let masterPos = [0, -0.45, -1.0];

    // LOADING BASELINE, MATERIAL AND ROTATION
    fbxLoader.load(
		'/MESH_02.fbx',
        (fbx) => {
            const model = fbx.children[0];
            console.log(model);
            model.scale.set(masterScale, masterScale, masterScale);
            meshBaseLine = model;
            console.log(meshBaseLine.length);    
            for (let i = 0; i < meshBaseLine.children.length; i++) {
                rotationSpeed.push(randomRotationIncrement());
                scaleFragment.push(meshBaseLine.children[i].scale);
                directionX.push(-1), directionY.push(-1), directionZ.push(-1);
                meshBaseLine.children[i].material.metalness = 0;
                //meshBaseLine.children[i].material.premultipliedAlpha = true;
                //meshBaseLine.children[i].material.blending = THREE.AdditiveBlending;
                //meshBaseLine.children[i].material.transparent = true;
                meshBaseLine.children[i].material.alphaTest = 0.5;
                //meshBaseLine.children[i].material.wireframe = true;
                meshBaseLine.children[i].material.wireframeLinewidth = 100;
                //meshBaseLine.children[i].material.color.setHex(0x101010);
                meshBaseLine.children[i].material.side = THREE.DoubleSide;
            }
            console.log(rotationSpeed.length);
            //console.log(scaleFragment);
            meshBaseLine.position.set(masterPos[0],masterPos[1],masterPos[2]).applyMatrix4(controller.matrixWorld);
            //meshBaseLine.quaternion.setFromRotationMatrix(controller.matrixWorld);
            meshBaseLine.rotation.y = Math.PI;
            scene.add(meshBaseLine);
		}
    );




    // CONTROL SPEED OF ANIMATION
    function Speed() {
        return Math.random(0, 10) * 0.01;
        //return 0.001;
    }

    // Detects finger on the screen
    function onSelect() {
        //place the objects
        meshSmallDetail.position.set(masterPos[0], masterPos[1], masterPos[2]).applyMatrix4(controller.matrixWorld);
        meshSmallDetail.quaternion.setFromRotationMatrix(controller.matrixWorld);
        //meshSmallDetail.rotation.x = Math.PI / 2;
        scene.add(meshSmallDetail);
	}

    // final setup
	controller = renderer.xr.getController(0);
    controller.addEventListener('select', onSelect);
	scene.add(controller);
	window.addEventListener('resize', onWindowResize);

}

function handleScale() {
    this.scaleFactor *= 1 + event.detail.spreadChange / event.detail.startSpread;

    this.scaleFactor = Math.min(
        Math.max(this.scaleFactor, this.data.minScale),
        this.data.maxScale
    );
    let Scalefactor = [this.scaleFactor, this.scaleFactor, this.scaleFactor,];
    let initScaleBase = meshBaseLine.scale;
    meshBaseLine.scale = initScaleBase + Scalefactor;
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}

// render every frame
function animate() {
	renderer.setAnimationLoop(render);
}

// render function
function render() {
	renderer.render(scene, camera);
}

// random rotation to every bone
function animation() {
        
    if (meshBaseLine != null) {
        meshBaseLine.rotation.y += 0.002;
        for (let i = 0; i < meshBaseLine.children.length; i++) {
            meshBaseLine.children[i].rotation.y += rotationSpeed[i];
        }
    }
}

// creates a random rotation increment
function randomRotationIncrement() {
    let direction = (Math.random(0, 1) > 0.5) ? 1 : -1;
    return Math.random(0, 2) * 0.001 * direction;
}


// Time
const clock = new THREE.Clock()
var startFrame = true;
let previousTime = 0;
let timeToUpdate = 1;
const tick = () => {
	//get time
	const elapsedTime = clock.getElapsedTime();
	const deltaTime = elapsedTime - previousTime;
	previousTime = elapsedTime;

    // animates the rotation of the bones
    animation();

    // update all animations
    if (mixerBase != null) {
        mixerBase.update(deltaTime);
    }

	// Call tick again on the next frame
	window.requestAnimationFrame(tick)
	if (startFrame) {
		startFrame = false;
	}
}


init();
animate();
tick();
